import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/route_manager.dart';

import 'routes/app_routes.dart';
import 'services/environment.dart';
import 'share/i18n/localization_service.dart';
import 'theme/theme_constants.dart';
import 'theme/theme_manager.dart';

Future<void> main() async {
  // await dotenv.load(fileName: Environment.fileName);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  static final ThemeManager _themeManager = ThemeManager();
  initFunction() {
    Get.updateLocale(const Locale('vi', 'VN'));
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(428, 926),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) => GetMaterialApp(
        title: 'Food',
        fallbackLocale: const Locale('en', 'US'),
        onInit: initFunction,
        translations: Languages(),
        debugShowCheckedModeBanner: false,
        getPages: AppRouterPages.getPage,
        initialRoute: AppRouterPages.initial,
        unknownRoute: AppRouterPages.notFound,
        theme: lightTheme,
        darkTheme: darkTheme,
        themeMode: _themeManager.themeMode,
      ),
    );
  }
}
