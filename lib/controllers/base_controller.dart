import 'package:get/get.dart';
import 'package:intl/intl.dart';

class BaseController extends GetxController {
  final formatCurrency =
      NumberFormat.simpleCurrency(locale: "vi_VN", decimalDigits: 3);
}
