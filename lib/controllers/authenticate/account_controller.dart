import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../routes/app_path.dart';
import '../base_controller.dart';

class AccountController extends BaseController {
  @override
  void onInit() {
    // data = productDonuts;
    super.onInit();
  }

  submitLogOut() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.clear();
    Get.offAllNamed(RoutesPath.login);
  }
}
