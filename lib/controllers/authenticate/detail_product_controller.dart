import 'dart:convert';
import 'package:food/share/constants/keys.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/products/response/product_response.dart';
import '../../routes/app_path.dart';
import '../base_controller.dart';

class DetailProductController extends BaseController {
  final RxInt _count = 1.obs;
  int get count => _count.value;
  final checkerMap = RxMap();
  final _toppingList = RxList<ToppingsResponse>([]);
  List<ToppingsResponse> get toppingList => _toppingList;
  final _optionsList = RxList<OptionsResponse>([]);
  List<OptionsResponse> get optionsList => _optionsList;
  final _totalPrice = RxInt(0);
  int get totalPrice => _totalPrice.value;
  final _countDisableButton = RxInt(0);
  int get countDisableButton => _countDisableButton.value;
  final _priceDefault = RxDouble(0);
  double get priceDefault => _priceDefault.value;
  late final itemProduct;

  ///update product
  final _indexUpdateProduct = RxInt(0);
  int get indexUpdateProduct => _indexUpdateProduct.value;

  /// hanlde select option product
  void changeOption(
    OptionsResponse itemOptions,
    OptionResponse itemOption,
    ProductResponse itemProduct,
  ) {
    int index = optionsList.indexOf(itemOptions);
    OptionsResponse items = optionsList[index];
    int subIndex = items.option!.indexOf(itemOption);
    for (var i = 0; i < items.option!.length; i++) {
      items.option![i].status = false;
    }
    items.option![subIndex].status = true;

    handlePriceTotal();
    update(['option_product']);
  }

  /// handle select toppings
  void changeToppings(ToppingsResponse toppingItems) {
    int index = toppingList.indexOf(toppingItems);
    if (toppingList[index].status) {
      _toppingList[index].status = false;
    } else {
      _toppingList[index].status = true;
    }
    handlePriceTotal();
    update(['change_option']);
  }

  /// hanlde add count product
  void addCount() {
    if (_count.value < 99) {
      _count.value++;
      handlePriceTotal();
    }
  }

  /// hanlde minust count product
  void minusCount() {
    if (_count.value != 1) {
      _count.value--;
      handlePriceTotal();
    }
  }

  /// handle price total
  handlePriceTotal() {
    double totalPriceOption = 0;
    double totalPriceTopping = 0;
    _countDisableButton.value = 0;
    for (var i = 0; i < optionsList.length; i++) {
      List<OptionResponse>? optionListTemp = optionsList[i].option;
      for (var j = 0; j < optionListTemp!.length; j++) {
        if (optionListTemp[j].status && optionListTemp[j].price != null) {
          totalPriceOption += optionListTemp[j].price!;
        }
        if (optionListTemp[j].status) {
          _countDisableButton.value++;
          break;
        }
      }
    }
    for (var i = 0; i < toppingList.length; i++) {
      if (toppingList[i].status) {
        totalPriceTopping += toppingList[i].price!;
      }
    }
    _totalPrice.value =
        ((priceDefault + totalPriceOption + totalPriceTopping) * count).toInt();
  }

  /// hanlde add product in cart
  Future<void> addCart(ProductResponse items) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    List<String> listProduct = localStorage.getStringList('cart_product') ?? [];

    Map<String, dynamic> product = {};
    items.toJson().forEach((key, value) {
      product[key] = value;
    });

    product['toppings'] = toppingList;
    product['options'] = optionsList;
    product['totalPrice'] = 0;
    product['quantily'] = 0;

    //check product in cart
    bool isAddProductInCart = false;
    for (var i = 0; i < listProduct.length; i++) {
      var convertStringToMap = jsonDecode(listProduct[i]);
      var quantilyTemp = convertStringToMap['quantily'];
      var totalPriceTemp = convertStringToMap['totalPrice'];
      convertStringToMap['quantily'] = 0;
      convertStringToMap['totalPrice'] = 0;
      if (await compareProductInCart(product, convertStringToMap)) {
        convertStringToMap['quantily'] = quantilyTemp + count;
        convertStringToMap['totalPrice'] = totalPriceTemp + totalPrice;
        listProduct[i] = jsonEncode(convertStringToMap);
        isAddProductInCart = true;
        if (indexUpdateProduct != -1) {
          listProduct.removeAt(indexUpdateProduct);
        }
        break;
      }
    }
    if (!isAddProductInCart) {
      product['totalPrice'] = totalPrice;
      product['quantily'] = count;
      var convertMapToString = jsonEncode(product);
      if (indexUpdateProduct != -1) {
        listProduct.removeAt(indexUpdateProduct);
      }
      listProduct.insert(0, convertMapToString);
    }

    for (var x = 0; x < listProduct.length; x++) {
      print(listProduct[x]);
    }
    localStorage.setStringList(Constants.cartProduct, listProduct);

    if (indexUpdateProduct == -1) {
      Get.offAllNamed(RoutesPath.home);
    } else {
      Get.offAllNamed(RoutesPath.cart);
    }
  }

  compareProductInCart(data1, data2) {
    var convertMapToStringData1 = jsonEncode(data1);
    var convertMapToStringData2 = jsonEncode(data2);

    if (convertMapToStringData1 == convertMapToStringData2) {
      return true;
    } else {
      return false;
    }
  }

  getTypeUpdate() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    _indexUpdateProduct.value =
        localStorage.getInt(Constants.typeUpdateProduct) ?? -1;
    print(indexUpdateProduct);
  }

  @override
  void onInit() {
    getTypeUpdate();
    var price = Get.arguments.price;
    ProductResponse items = Get.arguments;
    _totalPrice.value = price.toInt();
    _priceDefault.value = price;
    if (items.toppings != null && items.toppings!.isNotEmpty) {
      for (var i = 0; i < items.toppings!.length; i++) {
        var toObj = items.toppings![i].toJson();
        ToppingsResponse toppingItem = ToppingsResponse.fromJson(toObj);
        _toppingList.add(toppingItem);
      }
    }
    if (items.options != null && items.options!.isNotEmpty) {
      for (var i = 0; i < items.options!.length; i++) {
        var toObj = items.options![i].toJson();
        List toObjChild = [];
        for (var j = 0; j < items.options![i].option!.length; j++) {
          toObjChild.add(items.options![i].option![j].toJson());
          if (items.options![i].option![j].status) {
            _countDisableButton.value++;
          }
        }
        toObj['option'] = toObjChild;
        OptionsResponse optionItem = OptionsResponse.fromJson(toObj);
        _optionsList.add(optionItem);
      }
    }
    super.onInit();
  }

  disport() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    if (indexUpdateProduct != -1) {
      localStorage.setInt(Constants.typeUpdateProduct, -1);
    }
  }

  @override
  void onClose() {
    disport();
    super.onClose();
  }
}
