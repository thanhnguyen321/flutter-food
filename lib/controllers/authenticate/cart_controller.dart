import 'dart:convert';

import 'package:food/share/constants/keys.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/core_controller.dart';
import '../../routes/app_path.dart';
import '../base_controller.dart';

class CartController extends BaseController {
  List<ProductResponse> data = [];
  final _priceDefault = RxInt(0);
  int get priceDefault => _priceDefault.value;

  getData() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    List<String> listProduct =
        localStorage.getStringList(Constants.cartProduct) ?? [];

    List<ProductResponse> tempList = [];
    for (var i = 0; i < listProduct.length; i++) {
      print(listProduct[i]);
      var convertStringToMap = jsonDecode(listProduct[i]);
      ProductResponse items = ProductResponse.fromJson(convertStringToMap);
      tempList.add(items);
    }

    data = tempList;
    update();
  }

  addOrMinusProduct(count, items) {
    int index = data.indexOf(items);
    data[index].quantily = count;
    update();
  }

  removeProduct(items) async {
    int index = data.indexOf(items);
    data.removeAt(index);
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    List<String> dataTemp = [];
    for (var i = 0; i < data.length; i++) {
      dataTemp.add(jsonEncode(data[i]));
    }
    localStorage.setStringList(Constants.cartProduct, dataTemp);
    update();
  }

  updateProduct(items) async {
    int index = data.indexOf(items);
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setInt(Constants.typeUpdateProduct, index);
    Get.toNamed(RoutesPath.detailProduct, arguments: items);
  }

  @override
  void onInit() {
    getData();
    super.onInit();
  }
}
