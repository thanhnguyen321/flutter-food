import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:food/controllers/authenticate/cart_controller.dart';
import 'package:get/get.dart';

import '../../gen/assets.gen.dart';
import '../../models/core_controller.dart';
import '../base_controller.dart';

class HomeController extends BaseController {
  List<ProductResponse> data = [];
  List<ProductResponse> donutData = [];
  List<ProductResponse> bergerData = [];
  List<ProductResponse> cakeData = [];
  List<ProductResponse> milkTeaData = [];
  Map<String, List> dataMap = RxMap({});
  SuggestionsBoxController boxController = SuggestionsBoxController();

  RxInt _currentIndex = RxInt(0);
  int get currentIndex => _currentIndex.value;

  RxInt _currentCart = RxInt(0);
  int get currentCart => _currentCart.value;

  RxInt _currentTabTop = RxInt(0);
  int get currentTabTop => _currentTabTop.value;

  onChangeCurren(value) {
    _currentIndex.value = value;
  }

  onChangeTabTop(value) {
    _currentTabTop.value = value;
  }

  Future<void> getData() async {
    final String response =
        await rootBundle.loadString(Assets.json.listProduct);

    final dataTemp = await json.decode(response);
    data = List<ProductResponse>.from(
      dataTemp.map(
        (items) => ProductResponse.fromJson(items),
      ),
    );
  }

  /// data search top bar
  List<ProductResponse> getSuggestions(String query) {
    return List.of(data).where((user) {
      final userLower = user.title!.toLowerCase();
      final queryLower = query.toLowerCase();

      return userLower.contains(queryLower);
    }).toList();
  }

  /// close box top bar
  closeSearchBox() {
    boxController.close();
    update(['search_box']);
  }

  @override
  void onInit() async {
    await getData();
    _currentIndex.value = 0;
    for (var element in data) {
      switch (element.type) {
        case '1':
          donutData.add(element);
          break;
        case '2':
          bergerData.add(element);
          break;
        case '3':
          cakeData.add(element);
          break;
        case '4':
          milkTeaData.add(element);
          break;
        default:
      }
    }
    final cartController = await Get.put(CartController());
    _currentCart.value = cartController.data.length;

    update();
    super.onInit();
  }

  changeLike(index) {
    data[index].like = !data[index].like!;
    update();
  }
}

class Cake {
  String? title;
  Cake({this.title});

  static Cake fromJson(Map<String, dynamic> json) => Cake(
        title: json['title'],
      );
}
