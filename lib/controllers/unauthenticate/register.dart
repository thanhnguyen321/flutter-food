import 'package:flutter/material.dart';
import 'package:food/controllers/base_controller.dart';
import 'package:food/share/constants/keys.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterController extends BaseController {
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController rePasswordController = TextEditingController();
  final RxString _errUserName = ''.obs;
  String get errUserName => _errUserName.value;
  final RxString _errPassword = ''.obs;
  String get errPassword => _errPassword.value;
  final RxString _errRePassword = ''.obs;
  String get errRePassword => _errRePassword.value;

  void unErrInput(String value) {
    if (value.isNotEmpty) {
      _errUserName.value = '';
      _errPassword.value = '';
      _errRePassword.value = '';
    }
    update();
  }

  submitRegister() async {
    if (userNameController.text.isEmpty) {
      _errUserName.value = 'please_enter'.tr;
      update();
      return null;
    } else {
      _errUserName.value = '';
    }
    if (passwordController.text.isEmpty) {
      _errPassword.value = 'please_enter'.tr;
      update();
      return null;
    } else {
      _errPassword.value = '';
    }
    print(passwordController.text != rePasswordController.text);
    if (passwordController.text != rePasswordController.text) {
      _errRePassword.value = 'err_re_password'.tr;
      update();
      return null;
    } else {
      _errRePassword.value = '';
    }

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setStringList(Constants.account,
        [(userNameController.text), (passwordController.text)]);
    Get.back();
  }
}
