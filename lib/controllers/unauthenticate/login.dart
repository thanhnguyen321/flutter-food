import 'package:flutter/material.dart';
import 'package:food/routes/app_path.dart';
import 'package:food/share/constants/keys.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../base_controller.dart';

class LoginController extends BaseController {
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final RxString _errUserName = ''.obs;
  String get errUserName => _errUserName.value;
  final RxString _errPassword = ''.obs;
  String get errPassword => _errPassword.value;

  void unErrInput(String value) {
    if (value.isNotEmpty) {
      _errUserName.value = '';
      _errPassword.value = '';
    }
    update();
  }

  submitLogin() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    if (userNameController.text == '') {
      _errUserName.value = 'please_enter'.tr;
      update();
      return null;
    }
    if (passwordController.text == '') {
      _errPassword.value = 'please_enter'.tr;
      update();
      return null;
    }
    if (userNameController.text == 'admin' &&
        passwordController.text == '123') {
      localStorage.setString(Constants.token, 'login');
      Get.toNamed(RoutesPath.home);
    } else {
      Get.snackbar('notification'.tr, 'err_acccount'.tr,
          duration: const Duration(seconds: 2),
          snackPosition: SnackPosition.TOP,
          colorText: Colors.white,
          backgroundColor: Colors.red);
    }

    var account = localStorage.getStringList(Constants.account);
    if (account != null) {
      if (userNameController.text == account[0] &&
          passwordController.text == account[1]) {
        localStorage.setString(Constants.token, 'login');
        Get.toNamed(RoutesPath.home);
      } else {
        Get.snackbar('notification'.tr, 'err_acccount'.tr,
            duration: const Duration(seconds: 2),
            snackPosition: SnackPosition.TOP,
            colorText: Colors.white,
            backgroundColor: Colors.red);
      }
    }
  }
}
