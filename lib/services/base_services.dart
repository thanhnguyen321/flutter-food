import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

String baseUrl = '';

class BaseClient {
  var client = http.Client();

  //POST
  Future<dynamic> post(
    String api, {
    bool requiresAuthToken = false,
    dynamic params,
  }) async {
    var url = Uri.parse(baseUrl + api);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var payload = json.encode(params);
    var token = prefs.getString('token');

    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': requiresAuthToken ? 'Bearer $token' : ''
    };
    var response = await client.post(url, body: payload, headers: headers);
    if (response.statusCode == 200) {
      return response.body;
    }
  }

  /// GET
  Future<dynamic> get(String api, {Map<String, dynamic>? params}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse(baseUrl + api);
    var token = prefs.getString('token');

    if (params != null) {
      url = url.replace(queryParameters: params);
    }
    var headers = {
      'Accept': 'application/json',
    };
    if (token != null) {
      headers.addAll({'Authorization': 'Bearer $token'});
    }
    var response = await client.get(url, headers: headers);
    if (response.statusCode == 200) {
      return jsonDecode(utf8.decode(response.bodyBytes));
    }
  }

//PUT
  Future<dynamic> put(String api, Map<String, dynamic> params) async {
    var url = Uri.parse(baseUrl + api);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var payload = json.encode(params);

    var token = prefs.getString('token');

    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };
    if (token != null) {
      headers.addAll({'Authorization': 'Bearer $token'});
    }

    var response = await client.put(url, body: payload, headers: headers);
    print(response.statusCode);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      print(response.toString());
    }
  }
}
