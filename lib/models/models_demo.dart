class Data {
  String name;
  int age;
  List<Cruss> cruss;
  String? address;
  Data(
      {required this.name,
      required this.age,
      required this.cruss,
      this.address});

  Map<String, dynamic> toJson() => {
        'name': name,
        'age': age,
      };
}

class Cruss {
  String name;
  int age;
  Cruss({required this.name, required this.age});
}

List data = [
  Data(
    name: 'thanh',
    age: 20,
    cruss: [
      Cruss(name: 'Thanh', age: 18),
    ],
  ),
];
