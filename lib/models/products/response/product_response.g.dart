// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductResponse _$ProductResponseFromJson(Map<String, dynamic> json) =>
    ProductResponse(
      title: json['title'] as String? ?? '',
      type: json['type'] as String? ?? '',
      id: json['id'] as String? ?? '',
      price: (json['price'] as num?)?.toDouble(),
      image: json['image'] as String? ?? '',
      options: (json['options'] as List<dynamic>?)
          ?.map((e) => OptionsResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      toppings: (json['toppings'] as List<dynamic>?)
          ?.map((e) => ToppingsResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      description: json['description'] as String?,
      sale: (json['sale'] as num?)?.toDouble(),
      rate: (json['rate'] as num?)?.toDouble() ?? 0,
      quantily: json['quantily'] as int? ?? 0,
      totalPrice: (json['totalPrice'] as num?)?.toDouble(),
      status: json['status'] as bool? ?? true,
      like: json['like'] as bool? ?? false,
      isCart: json['isCart'] as bool? ?? false,
    );

Map<String, dynamic> _$ProductResponseToJson(ProductResponse instance) =>
    <String, dynamic>{
      'title': instance.title,
      'image': instance.image,
      'id': instance.id,
      'type': instance.type,
      'price': instance.price,
      'rate': instance.rate,
      'totalPrice': instance.totalPrice,
      'sale': instance.sale,
      'status': instance.status,
      'like': instance.like,
      'isCart': instance.isCart,
      'quantily': instance.quantily,
      'options': instance.options,
      'toppings': instance.toppings,
      'description': instance.description,
    };

OptionsResponse _$OptionsResponseFromJson(Map<String, dynamic> json) =>
    OptionsResponse(
      title: json['title'] as String,
      option: (json['option'] as List<dynamic>?)
          ?.map((e) => OptionResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      value: json['value'] as String? ?? '',
      quantily: json['quantily'] as int?,
    );

Map<String, dynamic> _$OptionsResponseToJson(OptionsResponse instance) =>
    <String, dynamic>{
      'title': instance.title,
      'value': instance.value,
      'quantily': instance.quantily,
      'option': instance.option,
    };

OptionResponse _$OptionResponseFromJson(Map<String, dynamic> json) =>
    OptionResponse(
      title: json['title'] as String,
      price: (json['price'] as num?)?.toDouble(),
      status: json['status'] as bool? ?? false,
    );

Map<String, dynamic> _$OptionResponseToJson(OptionResponse instance) =>
    <String, dynamic>{
      'title': instance.title,
      'status': instance.status,
      'price': instance.price,
    };

ToppingsResponse _$ToppingsResponseFromJson(Map<String, dynamic> json) =>
    ToppingsResponse(
      title: json['title'] as String,
      price: (json['price'] as num?)?.toDouble(),
      status: json['status'] as bool? ?? false,
    );

Map<String, dynamic> _$ToppingsResponseToJson(ToppingsResponse instance) =>
    <String, dynamic>{
      'title': instance.title,
      'price': instance.price,
      'status': instance.status,
    };
