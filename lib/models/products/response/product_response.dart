import 'package:json_annotation/json_annotation.dart';
part 'product_response.g.dart';

enum ProductTypes {
  donut('1'),
  berger('2'),
  cake('3'),
  milkTea('4');

  const ProductTypes(this.value);
  final String value;
}

@JsonSerializable()
class ProductResponse {
  String? title, image, id, type;
  double? price, rate, totalPrice, sale;
  bool? status, like, isCart;
  int quantily;
  List<OptionsResponse>? options;
  List<ToppingsResponse>? toppings;
  String? description;

  ProductResponse({
    this.title = '',
    this.type = '',
    this.id = '',
    this.price,
    this.image = '',
    this.options,
    this.toppings,
    this.description,
    this.sale,
    this.rate = 0,
    this.quantily = 0,
    this.totalPrice,
    this.status = true,
    this.like = false,
    this.isCart = false,
  });
  factory ProductResponse.fromJson(Map<String, dynamic> json) =>
      _$ProductResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProductResponseToJson(this);
}

@JsonSerializable()
class OptionsResponse {
  String title;
  String value;
  int? quantily;
  List<OptionResponse>? option = [];

  OptionsResponse({
    required this.title,
    required this.option,
    this.value = '',
    this.quantily,
  });
  factory OptionsResponse.fromJson(Map<String, dynamic> json) =>
      _$OptionsResponseFromJson(json);
  Map<String, dynamic> toJson() => _$OptionsResponseToJson(this);
}

@JsonSerializable()
class OptionResponse {
  String title;
  bool status;
  double? price;

  OptionResponse({
    required this.title,
    this.price,
    this.status = false,
  });
  factory OptionResponse.fromJson(Map<String, dynamic> json) =>
      _$OptionResponseFromJson(json);
  Map<String, dynamic> toJson() => _$OptionResponseToJson(this);
}

@JsonSerializable()
class ToppingsResponse {
  String title;
  double? price;
  bool status;

  ToppingsResponse({
    required this.title,
    required this.price,
    this.status = false,
  });
  factory ToppingsResponse.fromJson(Map<String, dynamic> json) =>
      _$ToppingsResponseFromJson(json);
  Map<String, dynamic> toJson() => _$ToppingsResponseToJson(this);
}
