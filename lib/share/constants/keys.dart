class Constants {
  /// local storage
  static String token = 'token';
  static String account = 'account';
  static String typeUpdateProduct = 'type_update_product';
  static String cartProduct = 'cart_product';

  /// image local
  static String logo = 'assets/images/logo.png';
  static String imgDefault = 'assets/images/imgDefault.png';
}
