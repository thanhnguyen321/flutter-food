String removeUnicode(String text) {
  String string1 =
      'áàảãạâấầẩẫậăắằẳẵặđéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵ';
  String string2 =
      'aaaaaaaaaaaaaaaaadeeeeeeeeeeeiiiiiooooooooooooooooouuuuuuuuuuuyyyyy';

  for (int i = 0; i < string1.length; i++) {
    text = text.replaceAll(string1[i], string2[i]);
    text = text.replaceAll(string1[i].toUpperCase(), string2[i].toUpperCase());
  }
  return text;
}
