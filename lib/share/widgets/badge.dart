import 'package:flutter/material.dart';

class BadgeCustom extends StatelessWidget {
  const BadgeCustom({required this.child, this.label = 0, super.key});
  final Widget child;
  final int label;

  @override
  Widget build(BuildContext context) {
    String labelString = '';
    if (label != 0) {
      labelString = label.toString();
    }
    if (label >= 100) {
      labelString = '99+';
    }

    double padding = label < 10
        ? 7
        : label < 100
            ? 3
            : 5;
    return Stack(
      children: [
        child,
        Positioned(
          top: 0,
          right: 0,
          child: Container(
            padding: EdgeInsets.all(padding),
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.red,
            ),
            child: Text(
              labelString,
              style: const TextStyle(fontSize: 12),
            ),
          ),
        )
      ],
    );
  }
}
