import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SwitchLanguage extends StatefulWidget {
  const SwitchLanguage({super.key});

  @override
  State<SwitchLanguage> createState() => _SwitchLanguageState();
}

class _SwitchLanguageState extends State<SwitchLanguage> {
  bool vnLanguage = true;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () {
          Locale locale;
          setState(() {
            vnLanguage = !vnLanguage;
          });
          if (vnLanguage) {
            locale = const Locale('vi', 'VN');
          } else {
            locale = const Locale('en', 'EN');
          }
          Get.updateLocale(locale);
        },
        child: Container(
          width: 52,
          height: 28,
          decoration: BoxDecoration(
              color: Colors.green, borderRadius: BorderRadius.circular(15)),
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: const [
                    Text(
                      'EN',
                      style: TextStyle(color: Colors.black, fontSize: 14),
                    ),
                    Text(
                      'VN',
                      style: TextStyle(color: Colors.black, fontSize: 14),
                    ),
                  ],
                ),
              ),
              AnimatedAlign(
                alignment:
                    vnLanguage ? Alignment.centerLeft : Alignment.centerRight,
                duration: const Duration(seconds: 1),
                curve: Curves.fastOutSlowIn,
                child: Container(
                  height: 25,
                  width: 25,
                  decoration: const BoxDecoration(
                      color: Colors.grey, shape: BoxShape.circle),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
