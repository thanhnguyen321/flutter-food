import 'package:flutter/material.dart';

class RatingBarCustom extends StatelessWidget {
  const RatingBarCustom({
    this.size,
    this.onChange,
    this.alignment = MainAxisAlignment.start,
    this.value = 0,
    super.key,
  });
  final double? size;
  final double value;
  final Function(double value)? onChange;
  final MainAxisAlignment alignment;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: alignment,
      children: List.generate(5, (index) {
        Color colorStar = value < index + 1 ? Colors.grey : Colors.amber;
        IconData icon = Icons.star;
        if (value > index + 0.3 && value <= index + 0.9) {
          icon = Icons.star_half_outlined;
          colorStar = Colors.amber;
        }

        return GestureDetector(
          child: Icon(
            icon,
            color: colorStar,
            size: size,
          ),
          onTap: () {
            onChange!(index.toDouble());
          },
        );
      }),
    );
  }
}
