import 'package:flutter/material.dart';

class GridViewCustom extends StatefulWidget {
  const GridViewCustom({
    required this.itemCount,
    required this.crossAxisCount,
    required this.itemBuider,
    super.key,
  });
  final Function(dynamic index) itemBuider;
  final int itemCount;
  final int crossAxisCount;
  @override
  State<GridViewCustom> createState() => _GridViewCustomState();
}

class _GridViewCustomState extends State<GridViewCustom> {
  final List<int> _data = [];
  @override
  void initState() {
    super.initState();
    for (var i = 0; i < widget.itemCount; i++) {
      _data.add(i);
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext ctx, BoxConstraints constraints) {
        return Wrap(
            children: _data.map((index) {
          return SizedBox(
            width: constraints.maxWidth / widget.crossAxisCount,
            child: widget.itemBuider(index),
          );
        }).toList());
      },
    );
  }
}
