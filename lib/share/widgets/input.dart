import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InputCustom extends StatefulWidget {
  const InputCustom({
    this.onChange,
    this.label,
    this.placeholder,
    this.type,
    this.contentErr,
    this.value,
    this.controller,
    this.background,
    this.maxLines,
    this.radius = 10,
    this.contentPadding = 13,
    this.fontSize = 16,
    this.focusNode,
    this.autofocus = false,
    this.textInputAction,
    super.key,
  });
  final TextEditingController? controller;
  final Function(String value)? onChange;
  final String? type, value, placeholder, contentErr;
  final Color? background;
  final TextInputAction? textInputAction;
  final double radius, contentPadding, fontSize;
  final bool autofocus;
  final FocusNode? focusNode;
  final int? maxLines;
  final Widget? label;

  @override
  State<InputCustom> createState() => _InputCustomState();
}

class _InputCustomState extends State<InputCustom> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var isNullErr = widget.contentErr == '' ? null : widget.contentErr;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        widget.label != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5), child: widget.label)
            : const SizedBox(),
        TextFormField(
          maxLines: widget.maxLines,
          autofocus: widget.autofocus,
          focusNode: widget.focusNode,
          textInputAction: widget.textInputAction,
          inputFormatters: widget.type == 'number'
              ? [FilteringTextInputFormatter.digitsOnly]
              : null,
          keyboardType: widget.type == 'number' ? TextInputType.number : null,
          controller: widget.controller,
          style: TextStyle(fontSize: widget.fontSize),
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(widget.radius),
              ),
              isCollapsed: true,
              filled: true,
              fillColor: widget.background,
              contentPadding: EdgeInsets.all(widget.contentPadding),
              hintText: widget.placeholder ?? '',
              hintStyle: const TextStyle(color: Color(0xFFC0C0C0)),
              errorText: isNullErr),
          onChanged: (value) {
            widget.onChange!(value);
          },
        )
      ],
    );
  }
}
