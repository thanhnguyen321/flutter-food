import 'package:flutter/material.dart';

class ButtonCustom extends StatelessWidget {
  const ButtonCustom({
    required this.title,
    required this.onChange,
    this.leftIcon,
    this.rightIcon,
    this.height,
    this.width,
    this.background,
    this.color,
    this.elevation,
    this.fontWeight,
    this.disable = false,
    this.radius = 5,
    this.fontSize = 16,
    this.paddingHorizontal = 5,
    this.align = MainAxisAlignment.center,
    super.key,
  });
  final String title;

  final Function onChange;
  final double paddingHorizontal, radius;
  final MainAxisAlignment align;
  final bool disable;
  final double? height, elevation, fontSize, width;
  final Widget? leftIcon, rightIcon;
  final FontWeight? fontWeight;
  final Color? background, color;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: background,
          disabledBackgroundColor: Colors.grey[400],
          elevation: elevation,
          padding: EdgeInsets.symmetric(horizontal: paddingHorizontal),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius),
          ),
        ),
        onPressed: disable
            ? null
            : () {
                onChange();
              },
        child: Row(
          mainAxisAlignment: align,
          children: [
            if (leftIcon != null)
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: leftIcon,
              ),
            Text(
              title,
              style: TextStyle(
                  fontSize: fontSize, color: color, fontWeight: fontWeight),
            ),
            if (rightIcon != null)
              Padding(
                padding: const EdgeInsets.only(left: 8),
                child: rightIcon,
              ),
          ],
        ),
      ),
    );
  }
}
