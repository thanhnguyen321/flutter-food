import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../constants/keys.dart';

class CircleAvatarCustom extends StatelessWidget {
  const CircleAvatarCustom({
    required this.networkImage,
    this.size = 40,
    this.borderSize = 1,
    this.isBorder = false,
    this.borderColor,
    super.key,
  });
  final String networkImage;
  final double size, borderSize;
  final bool isBorder;
  final Color? borderColor;

  @override
  Widget build(BuildContext context) {
    Color borderColors = const Color(0xFFDFDFDF);
    if (borderColor != null) {
      borderColors = borderColor as Color;
    }
    return Container(
      decoration: BoxDecoration(
          border: isBorder
              ? Border.all(color: borderColors, width: borderSize)
              : null,
          shape: BoxShape.circle),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(size / 2),
        child: FadeInImage(
          fit: BoxFit.cover,
          width: size,
          height: size,
          placeholder: AssetImage(Constants.imgDefault),
          imageErrorBuilder: (context, error, stackTrace) => ClipRRect(
            borderRadius: BorderRadius.circular(size / 2),
            child: Image.asset(
              Constants.imgDefault,
              fit: BoxFit.cover,
              width: size,
              height: size,
            ),
          ),
          image: NetworkImage(
            networkImage,
          ),
        ),
      ),
    );
  }
}
