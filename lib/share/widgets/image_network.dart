import 'package:flutter/material.dart';

import '../constants/keys.dart';

class ImageNetworkCustom extends StatelessWidget {
  const ImageNetworkCustom({
    super.key,
    required this.networkImage,
    this.height,
    this.width,
    this.onChange,
    this.showImage = false,
  });
  final String networkImage;
  final double? height, width;
  final Function? onChange;
  final bool showImage;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (showImage) {
          _showImage(networkImage, context);
        } else {
          onChange!();
        }
      },
      child: FadeInImage(
        fit: BoxFit.cover,
        width: width,
        height: height,
        placeholder: AssetImage(Constants.imgDefault),
        imageErrorBuilder: (context, error, stackTrace) => Image.asset(
          Constants.imgDefault,
          fit: BoxFit.cover,
          width: width,
          height: height,
        ),
        image: NetworkImage(
          networkImage,
        ),
      ),
    );
  }

  _showImage(url, context) {
    showDialog(
      context: context,
      barrierColor: Colors.black,
      builder: (context) => AlertDialog(
        contentPadding: const EdgeInsets.all(0),
        insetPadding: EdgeInsets.zero,
        content: GestureDetector(
          onTapCancel: () {
            Navigator.pop(context);
          },
          child: FadeInImage(
            fit: BoxFit.cover,
            placeholder: AssetImage(Constants.imgDefault),
            imageErrorBuilder: (context, error, stackTrace) => Image.asset(
              Constants.imgDefault,
              fit: BoxFit.cover,
            ),
            image: NetworkImage(
              networkImage,
            ),
          ),
        ),
      ),
    );
  }
}
