const Map<String, String> en = {
  //A
  //B
  'back_to_login': 'Back to login',
  //C
  //D
  //E
  'err_re_password': 'Password re-entered does not match',
  'err_acccount': 'Incorrect account or password',
  //F
  'forgot_password': 'Forgot password',
  //G
  //H
  //I
  //J
  //K
  //L
  'login': 'Login',
  //M
  //N
  'notification': 'Notification',
  'not_account': 'Do not have an account?',
  //O
  //P
  'password': 'Password',
  'please_enter': 'Please enter',
  //Q
  //R
  'register': 'Register',
  're_password': 'Re-password',
  //S
  //T
  //U
  'user_name': 'User name',
  //V
  //W
  //X
  //Y
  //Z
};
