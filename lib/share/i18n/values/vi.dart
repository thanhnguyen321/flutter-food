const Map<String, String> vi = {
  //A
  //B
  'back_to_login': 'Trở lại màn đăng nhập',
  //C
  //D
  //E
  'err_re_password': 'Mật khâu nhập lại không khớp',
  'err_acccount': 'Tài khoản hoặc mật khẩu không đúng',
  //F
  'forgot_password': 'Quên mật khẩu',
  //G
  //H
  //I
  //J
  //K
  //L
  'login': 'Đăng nhập',
  //M
  //N
  'notification': 'Thông báo',
  'not_account': 'Bạn chưa có tài khoản?',
  //O
  //P
  'password': 'Mật khẩu',
  'please_enter': 'Vui lòng nhập',
  //Q
  //R
  'register': 'Đăng ký',
  're_password': 'Nhập lại mật hẩu',
  //S
  //T
  //U
  'user_name': 'Tên đăng nhập',
  //V
  //W
  //X
  //Y
  //Z
};
