import 'package:get/get.dart';
import './values/en.dart';
import './values/vi.dart';

class Languages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {'vi_VN': vi, 'en_US': en};
}
