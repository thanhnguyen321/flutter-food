import 'package:food/routes/bindings/login_binding.dart';
import 'package:food/views/authenticate/page/cart_page/cart_page.dart';
import 'package:food/views/authenticate/screen/detail_product/detail_product.dart';
import 'package:food/views/unauthenticate/register.dart';
import 'package:get/get.dart';

import '../views/authenticate/page/home_page/home_page.dart';
import '../views/unauthenticate/login.dart';
import '../views/unauthenticate/splash.dart';
import 'app_path.dart';
import 'bindings/cart_binding.dart';
import 'bindings/detail_product_binding.dart';
import 'bindings/home_binding.dart';
import 'bindings/register_binding.dart';

class AppRouterPages {
  static String initial = RoutesPath.notFound;
  static final notFound =
      GetPage(name: Routes.notFound, page: () => const Splash());

  /// auth
  static List<GetPage> unAuthPages = [
    GetPage(
      name: Routes.login,
      page: () => const LoginPage(),
      binding: LoginBinding(),
    ),
    GetPage(
        name: Routes.register,
        page: () => const RegisterPage(),
        binding: RegisterBinding()),
  ];

  ///screen
  static List<GetPage> authPages = [
    GetPage(
      name: Routes.home,
      page: () => const HomePage(),
      binding: HomeBinding(),
      children: [],
    ),
    GetPage(
      name: Routes.cart,
      page: () => const CartPage(),
      binding: CartBinding(),
    ),
    GetPage(
      name: Routes.detailProduct,
      page: () => const ProductDetail(),
      binding: DetailProductBinding(),
    ),
  ];

  static List<GetPage> getPage = [
    ...authPages,
    ...unAuthPages,
  ];
}
