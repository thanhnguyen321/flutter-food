class Routes {
  /// unauthenticate
  static String login = '/login';
  static String register = '/register';

  /// authenticate
  static String home = '/home';
  static String cart = '/cart';
  static String detailProduct = '/detail-product';

  static String notFound = '/not-found';
}

class RoutesPath {
  /// unauthenticate
  static String login = Routes.login;
  static String register = Routes.register;
  static String notFound = Routes.notFound;

  /// authenticate
  static String home = Routes.home;
  static String cart = Routes.cart;
  static String detailProduct = Routes.detailProduct;
}
