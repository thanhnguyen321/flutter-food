import 'package:food/controllers/authenticate/account_controller.dart';
import 'package:get/get.dart';

import '../../controllers/authenticate/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get
      ..lazyPut<HomeController>(() => HomeController())
      ..put(AccountController());
  }
}
