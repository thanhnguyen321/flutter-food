import 'package:get/get.dart';

import 'package:food/controllers/unauthenticate/register.dart';

class RegisterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RegisterController>(() => RegisterController());
  }
}
