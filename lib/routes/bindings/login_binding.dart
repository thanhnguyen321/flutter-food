import 'package:get/get.dart';

import 'package:food/controllers/unauthenticate/login.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginController>(() => LoginController());
  }
}
