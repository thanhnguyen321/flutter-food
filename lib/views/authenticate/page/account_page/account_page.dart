import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../controllers/authenticate/account_controller.dart';

class AccountPage extends GetView<AccountController> {
  const AccountPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AccountController>(
      builder: (ctx) {
        return Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                  style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.green),
                  onPressed: ctx.submitLogOut,
                  child: const Text('Log out'))
            ],
          ),
        );
      },
    );
  }
}
