import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:food/share/widgets/input.dart';
import 'package:food/views/authenticate/page/demo.dart';
import 'package:food/views/authenticate/page/product_page/product_page.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../controllers/authenticate/home_controller.dart';

import '../../../../models/core_controller.dart';
import '../../../../routes/app_path.dart';
import '../../../../share/widgets/badge.dart';

import '../account_page/account_page.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({super.key});

  static final _tabs = [const ProductPage(), const AccountPage()];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: GestureDetector(
        onTap: () {
          controller.closeSearchBox();
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            leadingWidth: 10,
            title: GetBuilder<HomeController>(
              id: 'search_box',
              builder: (ctx) {
                return TypeAheadField<ProductResponse?>(
                  suggestionsBoxController: ctx.boxController,
                  textFieldConfiguration: const TextFieldConfiguration(
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.search),
                      contentPadding: EdgeInsets.all(1),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(15),
                          ),
                          borderSide: BorderSide(color: Colors.red)),
                      hintText: 'Search...',
                    ),
                  ),
                  suggestionsCallback: ctx.getSuggestions,
                  itemBuilder: (context, ProductResponse? suggestion) {
                    final user = suggestion!;
                    return ListTile(
                      leading: Container(
                        width: 60,
                        height: 60,
                        child: Image.network(
                          user.image ?? '',
                          fit: BoxFit.cover,
                        ),
                      ),
                      title: Text(user.title ?? ''),
                      subtitle: Text(ctx.formatCurrency.format(user.price)),
                    );
                  },
                  suggestionsBoxDecoration: const SuggestionsBoxDecoration(
                    constraints: BoxConstraints(
                      maxHeight: 500,
                    ),
                  ),
                  noItemsFoundBuilder: (context) => Container(
                    height: 100,
                    child: const Center(
                      child: Text(
                        'No Users Found.',
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                  ),
                  onSuggestionSelected: (ProductResponse? suggestion) {
                    final product = suggestion!;
                    Get.toNamed(RoutesPath.detailProduct, arguments: product);
                  },
                );
              },
            ),
            actions: [
              Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Obx(
                    () => BadgeCustom(
                      label: controller.currentCart,
                      child: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: IconButton(
                          onPressed: () {
                            Get.toNamed(RoutesPath.cart);
                          },
                          icon: Icon(
                            Icons.shopping_cart_outlined,
                            color: Colors.black,
                            size: 35.sp,
                          ),
                        ),
                      ),
                    ),
                  )),
            ],
          ),
          body: Obx(() {
            return _tabs[controller.currentIndex];
          }),
          bottomNavigationBar: Obx(
            () {
              return BottomNavigationBar(
                items: const <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                    icon: Icon(Icons.home),
                    label: 'Home',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.settings),
                    label: 'Settings',
                  ),
                ],
                currentIndex: controller.currentIndex,
                selectedItemColor: Colors.green,
                showSelectedLabels: true,
                onTap: (value) {
                  controller.onChangeCurren(value);
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
