import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import '../../../gen/assets.gen.dart';
import '../../../models/models_demo.dart';
import '../../../models/products/response/product_response.dart';
import '../../../share/helpers/remove_unicode.dart';

class Demo extends StatefulWidget {
  const Demo({super.key});

  @override
  State<Demo> createState() => _DemoState();
}

class _DemoState extends State<Demo> with WidgetsBindingObserver {
  static List<ProductResponse> dataProduct = [];
  @override
  void initState() {
    getData();

    WidgetsBinding.instance.addObserver(this);
    super.initState();
    print('init: $dataProduct');

    print(removeUnicode('Ngày đó tao toàn than thỞ'));
  }

  getData() async {
    final String response =
        await rootBundle.loadString(Assets.json.listProduct);

    final dataTemp = await json.decode(response);

    setState(() {
      dataProduct = List<ProductResponse>.from(
        dataTemp.map(
          (items) => ProductResponse.fromJson(items),
        ),
      );
    });
  }

  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('demo bakcground: $state');

    /// các trạng thái state:
    /// AppLifecycleState.inactive: trạng thái đa nhiệm
    /// AppLifecycleState.paused: trạng thái background
    /// AppLifecycleState.resumed: trạng thái quay trở lại app
    /// AppLifecycleState.detached: trạng thái thoát app
  }

  static List<ProductResponse> getSuggestions123(String query) {
    return List.of(dataProduct).where((user) {
      final userLower = user.title!.toLowerCase();
      final queryLower = query.toLowerCase();

      return userLower.contains(queryLower);
    }).toList();
  }

  @override
  void dispose() {
    print('demo dispose');
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('init99: $dataProduct');
    print('view');
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: TypeAheadField<ProductResponse?>(
            hideSuggestionsOnKeyboardHide: false,
            textFieldConfiguration: TextFieldConfiguration(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(),
                hintText: 'Search Username',
              ),
            ),
            suggestionsCallback: getSuggestions123,
            // suggestionsCallback: UserData.getSuggestions,
            itemBuilder: (context, ProductResponse? suggestion) {
              final user = suggestion!;

              return ListTile(
                leading: Container(
                  width: 60,
                  height: 60,
                  child: Image.network(
                    user.image ?? '',
                    fit: BoxFit.cover,
                  ),
                ),
                title: Text(user.title ?? ''),
              );
            },
            noItemsFoundBuilder: (context) => Container(
              height: 100,
              child: Center(
                child: Text(
                  'No Users Found.',
                  style: TextStyle(fontSize: 24),
                ),
              ),
            ),
            onSuggestionSelected: (ProductResponse? suggestion) {
              final user = suggestion!;
              print(user);
              // Navigator.of(context).push(MaterialPageRoute(
              //   builder: (context) => UserDetailPage(user: user),
              // ));
            },
          ),
        ),
      ),
    );
  }
}

class User {
  final String name;
  final String imageUrl;

  const User({
    required this.name,
    required this.imageUrl,
  });
}

// class UserData {
//   // static final faker = Faker();
//   static final List<User> users = List.generate(
//     50,
//     (index) => User(
//       name: 'toi la thanh $index',
//       imageUrl: 'https://source.unsplash.com/random?user+face&sig=$index',
//     ),
//   );
//   static List<User> getSuggestions(String query) {
//     return List.of(users).where((user) {
//       final userLower = user.name.toLowerCase();
//       final queryLower = query.toLowerCase();

//       return userLower.contains(queryLower);
//     }).toList();
//   }
// }
