import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../controllers/authenticate/cart_controller.dart';
import '../../../../models/core_controller.dart';
import '../../../../routes/app_path.dart';
import '../../widgets/add_or_minus_product.dart';

class CartPage extends GetView<CartController> {
  const CartPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Cart',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.green[100],
          centerTitle: false,
          leading: IconButton(
            onPressed: () {
              Get.offAllNamed(RoutesPath.home);
            },
            icon: const Icon(Icons.arrow_back_ios),
            color: Colors.black,
          ),
        ),
        body: GetBuilder<CartController>(
          builder: (ctx) {
            return Column(
              children: [
                const SizedBox(height: 10),
                ...ctx.data.map((ProductResponse items) {
                  return _cartItem(ctx, items);
                })
              ],
            );
          },
        ));
  }

  _cartItem(ctx, ProductResponse items) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      decoration: BoxDecoration(
          border: Border(
              bottom:
                  BorderSide(width: 5, color: Colors.grey.withOpacity(0.5)))),
      child: Row(
        children: [
          Image.network(
            '${items.image}',
            height: 100,
            width: 100,
          ),
          const SizedBox(width: 15),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '${items.title}',
                      style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        controller.removeProduct(items);
                      },
                      icon: Icon(Icons.close),
                    )
                  ],
                ),
                ...List.generate(items.options!.length, (index) {
                  var options = items.options![index];
                  String value = '';
                  for (var i = 0; i < options.option!.length; i++) {
                    if (options.option![i].status) {
                      value = options.option![i].title;
                    }
                  }
                  return Text('${options.title}: ${value}');
                }),
                ...items.toppings!.map(
                  (toppings) {
                    if (toppings.status) {
                      return Text('${toppings.title}');
                    }
                    return const SizedBox();
                  },
                ).toList(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        'Giá: ${controller.formatCurrency.format(items.totalPrice)}'),
                  ],
                ),
                Text('Số lượng: ${items.quantily}'),
                TextButton(
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    minimumSize: const Size(0, 30),
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ),
                  onPressed: () {
                    controller.updateProduct(items);
                  },
                  child: const Text(
                    'Chỉnh sửa',
                    style: TextStyle(color: Colors.green),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
