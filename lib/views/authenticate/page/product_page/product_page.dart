import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../controllers/authenticate/home_controller.dart';
import '../../../../models/core_controller.dart';
import '../../../../routes/app_path.dart';
import '../../../../share/widgets/circle_avatar.dart';
import '../../../../share/widgets/grid_view.dart';
import '../../../../share/widgets/rating_bar.dart';

class ProductPage extends GetView<HomeController> {
  const ProductPage({super.key});

  @override
  Widget build(BuildContext context) {
    final List<String> tabList = [
      'assets/images/donut.png',
      'assets/images/burger.png',
      'assets/images/cake.png',
      'assets/images/milkTea.png'
    ];
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
      child: Column(
        children: [
          TabBar(
            onTap: (value) {
              controller.onChangeTabTop(value);
            },
            indicatorColor: Colors.green,
            tabs: List.generate(
              tabList.length,
              (index) {
                return _myTab(tabList[index], index);
              },
            ),
            // [
            //   _myTab('assets/images/donut.png'),
            //   _myTab('assets/images/burger.png'),
            //   _myTab('assets/images/cake.png'),
            //   _myTab('assets/images/milkTea.png'),
            // ],
          ),
          const SizedBox(height: 10),
          Expanded(
            child: TabBarView(children: [
              SingleChildScrollView(
                child: GetBuilder<HomeController>(
                  init: HomeController(),
                  builder: (controller) {
                    return _listProduct(controller, controller.donutData,
                        ProductTypes.donut.value);
                  },
                ),
              ),
              SingleChildScrollView(
                child: GetBuilder<HomeController>(
                  init: HomeController(),
                  builder: (controller) {
                    return _listProduct(controller, controller.bergerData,
                        ProductTypes.berger.value);
                  },
                ),
              ),
              SingleChildScrollView(
                child: GetBuilder<HomeController>(
                  init: HomeController(),
                  builder: (controller) {
                    return _listProduct(controller, controller.cakeData,
                        ProductTypes.cake.value);
                  },
                ),
              ),
              SingleChildScrollView(
                child: GetBuilder<HomeController>(
                  init: HomeController(),
                  builder: (controller) {
                    return _listProduct(controller, controller.milkTeaData,
                        ProductTypes.milkTea.value);
                  },
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }

  Tab _myTab(String image, int index) {
    return Tab(
        height: 80,
        child: Obx(
          () {
            bool activeCurrent =
                controller.currentTabTop == index ? true : false;
            return Container(
              padding: const EdgeInsets.all(12),
              decoration: activeCurrent
                  ? BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.grey[200],
                      border: Border.all(color: const Color(0xFFDFDFDF)),
                    )
                  : null,
              child: Image.asset(
                image,
                color: activeCurrent ? Colors.green : Colors.grey,
              ),
            );
          },
        ));
  }

  _listProduct(controller, data, type) {
    if (data.length > 0) {
      return GridViewCustom(
        itemCount: data.length,
        crossAxisCount: 2,
        itemBuider: (index) {
          ProductResponse items = data[index];
          Color colorLike = items.like! ? Colors.red : Colors.grey;
          if (items.type == type) {
            return GestureDetector(
              onTap: () {
                Get.toNamed(RoutesPath.detailProduct, arguments: items);
              },
              child: Container(
                margin: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                    color: Colors.green[50],
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(10),
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                bottomLeft: Radius.circular(10),
                              )),
                          child: Text(
                              controller.formatCurrency.format(items.price)),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    CircleAvatarCustom(
                      networkImage: items.image ?? '',
                      size: 150,
                      isBorder: true,
                      borderColor: Colors.green[100],
                      borderSize: 2,
                    ),
                    const SizedBox(height: 10),
                    Text(items.title ?? ''),
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            controller.changeLike(index);
                          },
                          icon: Icon(
                            Icons.favorite,
                            color: colorLike,
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: RatingBarCustom(
                              size: 20,
                              value: items.rate ?? 0,
                              alignment: MainAxisAlignment.end,
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            );
          }
        },
      );
    } else {
      return const Tab(
        child: Center(
          child: CircularProgressIndicator(
            value: 0.1,
            color: Colors.green,
          ),
        ),
      );
    }
  }
}
