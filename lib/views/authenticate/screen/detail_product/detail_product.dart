import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:food/views/authenticate/screen/detail_product/widgets/info_product.dart';
import 'package:food/views/authenticate/screen/detail_product/widgets/options.dart';
import 'package:food/views/authenticate/screen/detail_product/widgets/topppings_detail_product.dart';
import 'package:get/get.dart';

import '../../../../controllers/authenticate/detail_product_controller.dart';
import '../../../../models/core_controller.dart';
import '../../../../share/widgets/button.dart';
import '../../../../share/widgets/image_network.dart';

class ProductDetail extends GetView<DetailProductController> {
  const ProductDetail({super.key});

  @override
  Widget build(BuildContext context) {
    final ProductResponse items = Get.arguments;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          items.title ?? '',
          style: const TextStyle(
            color: Colors.black,
          ),
        ),
        centerTitle: false,
        foregroundColor: Colors.black,
        backgroundColor: Colors.green[100],
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ImageNetworkCustom(
              networkImage: items.image ?? '',
              width: MediaQuery.of(context).size.width,
              height: 250.h,
            ),
            Column(
              children: [
                /// info product
                InfoProductDetailProduct(
                  items: items,
                ),
                ListView(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    /// toppings product
                    if (items.toppings != null && items.toppings!.isNotEmpty)
                      const ToppingsDetailProduct(),
                    SizedBox(height: 5.h),

                    /// options product
                    OptionsItemDetailProduct(
                      items: items,
                    ),
                    SizedBox(height: 30.h),

                    _addOrMinusProduct(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            'Tổng tiền:',
                            style: TextStyle(fontSize: 18),
                          ),
                          Obx(
                            () => Text(
                              controller.formatCurrency
                                  .format(controller.totalPrice),
                              style: const TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.green),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: kBottomNavigationBarHeight + 10,
            )
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Obx(
          () => ButtonCustom(
            disable:
                controller.countDisableButton != controller.optionsList.length,
            background: Colors.green,
            fontSize: 20,
            height: 40,
            fontWeight: FontWeight.w700,
            title: controller.indexUpdateProduct != -1
                ? 'Update product'
                : 'Add Cart',
            onChange: () {
              controller.addCart(items);
            },
          ),
        ),
      ),
    );
  }

  Row _addOrMinusProduct() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: () {
            controller.minusCount();
          },
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(), borderRadius: BorderRadius.circular(5)),
            child: const Icon(
              Icons.remove,
              size: 30,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Obx(
            () => Text(
              '${controller.count}',
              style: const TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.w600,
                  color: Colors.green),
            ),
          ),
        ),
        InkWell(
          onTap: () {
            controller.addCount();
          },
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(), borderRadius: BorderRadius.circular(5)),
            child: const Icon(
              Icons.add,
              size: 30,
            ),
          ),
        ),
      ],
    );
  }
}
