import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../controllers/authenticate/detail_product_controller.dart';
import '../../../../../models/core_controller.dart';

class OptionsItemDetailProduct extends GetView<DetailProductController> {
  const OptionsItemDetailProduct({required this.items, super.key});
  final ProductResponse items;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DetailProductController>(
      id: 'option_product',
      builder: (ctx) {
        return Column(
          children: List.generate(
            ctx.optionsList.length,
            (index) {
              OptionsResponse optionItems = ctx.optionsList[index];
              return Container(
                margin: const EdgeInsets.only(bottom: 5),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 2,
                      offset: const Offset(0, 3),
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10, top: 10),
                      child: Text(
                        optionItems.title,
                        style: TextStyle(
                            fontSize: 20.sp, fontWeight: FontWeight.w600),
                      ),
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: optionItems.option!.length,
                      itemBuilder: (context, subIndex) {
                        OptionResponse optionSubItems =
                            optionItems.option![subIndex];
                        return CheckboxListTile(
                          value: optionSubItems.status,
                          onChanged: (value) {
                            ctx.changeOption(
                                optionItems, optionSubItems, items);
                          },
                          title: Text(optionSubItems.title),
                          secondary: Text(optionSubItems.price != null
                              ? ctx.formatCurrency.format(optionSubItems.price)
                              : ''),
                          controlAffinity: ListTileControlAffinity.leading,
                        );
                      },
                    ),
                  ],
                ),
              );
            },
          ),
        );
      },
    );
  }
}
