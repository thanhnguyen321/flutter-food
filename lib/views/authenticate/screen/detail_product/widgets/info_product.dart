import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../controllers/authenticate/detail_product_controller.dart';
import '../../../../../models/core_controller.dart';
import '../../../../../share/widgets/rating_bar.dart';

class InfoProductDetailProduct extends GetView<DetailProductController> {
  const InfoProductDetailProduct({
    super.key,
    required this.items,
  });
  final ProductResponse items;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        SizedBox(height: 10.h),
        ...[
          Text(
            items.title ?? '',
            style: const TextStyle(fontSize: 26, fontWeight: FontWeight.w600),
          ),
          Text(
            controller.formatCurrency.format(items.price),
            style: const TextStyle(
                fontSize: 18, fontWeight: FontWeight.w500, color: Colors.green),
          ),
          RatingBarCustom(
            size: 20,
            value: items.rate ?? 0,
          ),
          Text(
            items.description ?? '',
          ),
        ].expand(
          (widget) => [
            widget,
            SizedBox(height: 5.h),
          ],
        )
      ]),
    );
  }
}
