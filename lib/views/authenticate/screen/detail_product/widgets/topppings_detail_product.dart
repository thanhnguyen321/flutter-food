import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../controllers/authenticate/detail_product_controller.dart';
import '../../../../../models/core_controller.dart';

class ToppingsDetailProduct extends GetView<DetailProductController> {
  const ToppingsDetailProduct({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 2,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: GetBuilder<DetailProductController>(
          id: 'change_option',
          builder: (ctx) {
            return ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: ctx.toppingList.length,
              itemBuilder: (context, index) {
                ToppingsResponse toppingItems = ctx.toppingList[index];
                bool status = ctx.toppingList[index].status;
                return Container(
                  decoration: const BoxDecoration(
                      border:
                          Border(bottom: BorderSide(color: Color(0xffdedede)))),
                  child: CheckboxListTile(
                    value: status,
                    onChanged: (value) {
                      print('demo');
                      ctx.changeToppings(toppingItems);
                    },
                    title: Text(toppingItems.title),
                    secondary: Text(toppingItems.price != null
                        ? ctx.formatCurrency.format(toppingItems.price)
                        : ''),
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                );
              },
            );
          }),
    );
  }
}
