import 'package:flutter/material.dart';

class AddOrMinusProduct extends StatelessWidget {
  const AddOrMinusProduct({
    required this.onChange,
    this.defaultValue = 0,
    this.skipCount = 1,
    this.maxCount = 99,
    this.minCount = 0,
    this.size = 30,
    this.horizontal = 20,
    super.key,
  });
  final int defaultValue;
  final int skipCount, maxCount, minCount;
  final double size, horizontal;
  final Function(int value) onChange;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: () {
            if (defaultValue > minCount) {
              onChange(defaultValue - skipCount);
            }
          },
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(), borderRadius: BorderRadius.circular(5)),
            child: Icon(
              Icons.remove,
              size: size,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: horizontal),
          child: Text(
            '$defaultValue',
            style: TextStyle(
              fontSize: size,
              fontWeight: FontWeight.w600,
              color: Colors.green,
            ),
          ),
        ),
        InkWell(
          onTap: () {
            if (defaultValue < maxCount) {
              onChange(defaultValue + skipCount);
            }
          },
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(), borderRadius: BorderRadius.circular(5)),
            child: Icon(
              Icons.add,
              size: size,
            ),
          ),
        ),
      ],
    );
  }
}
