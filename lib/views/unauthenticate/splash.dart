import 'package:flutter/material.dart';
import 'package:food/share/constants/keys.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../routes/app_path.dart';

class Splash extends StatefulWidget {
  const Splash({super.key});

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    _navigatorHome();
  }

  _navigatorHome() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(Constants.token);
    print(token);
    if (token != null && token.isNotEmpty) {
      Get.toNamed(RoutesPath.home);
    } else {
      Get.toNamed(RoutesPath.login);
    }
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('Splash app'),
      ),
    );
  }
}
