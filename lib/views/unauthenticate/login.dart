import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:food/controllers/unauthenticate/login.dart';
import 'package:food/routes/app_path.dart';
import 'package:food/share/constants/keys.dart';
import 'package:food/share/widgets/button.dart';
import 'package:food/share/widgets/input.dart';
import 'package:get/get.dart';

import '../../share/widgets/switch_language.dart';

class LoginPage extends GetView<LoginController> {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    double bottomRegister = AppBar().preferredSize.height - 20;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        toolbarHeight: 56,
        backgroundColor: Colors.transparent,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20).r,
            child: const SwitchLanguage(),
          )
        ],
      ),
      backgroundColor: Colors.green[40],
      body: SingleChildScrollView(
        child: GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20).r,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      Constants.logo,
                      fit: BoxFit.cover,
                      height: 250.h,
                      width: 500.w,
                    ),
                    GetBuilder<LoginController>(
                      builder: (controller) {
                        return Column(
                          children: [
                            InputCustom(
                              onChange: (value) {
                                controller.unErrInput(value);
                              },
                              background: Colors.white,
                              label: Text(
                                'user_name'.tr,
                                style: const TextStyle(fontSize: 16),
                              ),
                              fontSize: 16.sp,
                              placeholder: 'user_name'.tr,
                              controller: controller.userNameController,
                              contentErr: controller.errUserName,
                            ),
                            SizedBox(height: 20.h),
                            InputCustom(
                              onChange: (value) {
                                controller.unErrInput(value);
                              },
                              background: Colors.white,
                              label: Text(
                                'password'.tr,
                                style: TextStyle(fontSize: 16.sp),
                              ),
                              placeholder: 'password'.tr,
                              controller: controller.passwordController,
                              contentErr: controller.errPassword,
                            ),
                            SizedBox(height: 30.h),
                            ButtonCustom(
                              title: 'login'.tr,
                              fontWeight: FontWeight.w600,
                              fontSize: 18.sp,
                              height: 45.h,
                              background: Colors.green[300],
                              onChange: controller.submitLogin,
                            ),
                            SizedBox(height: 10.h),
                            TextButton(
                              onPressed: () {},
                              child: Text(
                                'forgot_password'.tr,
                                style: TextStyle(
                                  fontSize: 16.sp,
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    )
                  ],
                ),
                Positioned(
                  bottom: bottomRegister.h,
                  left: 0,
                  right: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('not_account'.tr),
                      TextButton(
                        onPressed: () {
                          Get.toNamed(RoutesPath.register);
                        },
                        child: Text('register'.tr),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
