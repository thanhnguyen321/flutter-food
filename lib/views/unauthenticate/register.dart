import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:food/controllers/unauthenticate/register.dart';
import 'package:get/get.dart';

import '../../share/widgets/button.dart';
import '../../share/widgets/input.dart';

class RegisterPage extends GetView<RegisterController> {
  const RegisterPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20).r,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                GetBuilder<RegisterController>(
                  builder: (controller) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'register'.tr,
                          style: const TextStyle(
                              fontSize: 36, fontWeight: FontWeight.w700),
                        ),
                        SizedBox(height: 50.h),
                        InputCustom(
                          onChange: (value) {
                            controller.unErrInput(value);
                          },
                          controller: controller.userNameController,
                          background: Colors.white,
                          label: Text(
                            'user_name'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          fontSize: 16.sp,
                          placeholder: 'user_name'.tr,
                          contentErr: controller.errUserName,
                        ),
                        SizedBox(height: 20.h),
                        InputCustom(
                          onChange: (value) {
                            controller.unErrInput(value);
                          },
                          background: Colors.white,
                          label: Text(
                            'password'.tr,
                            style: TextStyle(fontSize: 16.sp),
                          ),
                          placeholder: 'password'.tr,
                          controller: controller.passwordController,
                          contentErr: controller.errPassword,
                        ),
                        SizedBox(height: 20.h),
                        InputCustom(
                          onChange: (value) {
                            controller.unErrInput(value);
                          },
                          background: Colors.white,
                          label: Text(
                            're_password'.tr,
                            style: TextStyle(fontSize: 16.sp),
                          ),
                          placeholder: 're_password'.tr,
                          controller: controller.rePasswordController,
                          contentErr: controller.errRePassword,
                        ),
                        SizedBox(height: 30.h),
                        ButtonCustom(
                          title: 'register'.tr,
                          fontWeight: FontWeight.w600,
                          fontSize: 18.sp,
                          height: 45.h,
                          background: Colors.green[300],
                          onChange: controller.submitRegister,
                        ),
                        SizedBox(height: 10.h),
                        TextButton(
                          onPressed: () {
                            Get.back();
                          },
                          child: Text(
                            'back_to_login'.tr,
                            style: TextStyle(
                              fontSize: 16.sp,
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
